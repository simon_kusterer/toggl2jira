#!/usr/bin/env node
const moment = require('moment');
const fs = require('fs');
const _ = require('lodash');
const chalk = require('chalk');
const checkmarks = require('checkmarks');
const prettyMs = require('pretty-ms');
const chrono = require('chrono-node');
const argv = require('minimist')(process.argv.slice(2));
const configPath = require('userhome')('.toggl2jira.json');

if (!fs.existsSync(configPath)) {
    console.log(`missing config file: ${configPath}`);
    return;
}

const config = JSON.parse(fs.readFileSync(configPath));
const jira = require('./jira.js')(config);
const toggl = require('./toggl-api')(config);

// 1. get all worklogs from toggl, starting from a specific time
// 2. group them by issueId
// 3. get the jira worklogs for each issueId
// 4. filter out toggl worklogs if they were already added to jira
// 5. combine the remaining toggl worklogs: one worklog per day and per issue
// 6. add those new worklogs to jira

// TODO: pass in 'from' parameter from commanline

const from = chrono.parseDate(argv.from || 'today');

function getTogglWorklogs() {
    const startOfTheDay = moment(from).startOf('day').toDate();
    const now = moment().toDate();

    return toggl.getWorklog(startOfTheDay, now);
}

function groupTogglWorklogsByIssueKey(togglWorklogs) {
    return _.groupBy(togglWorklogs, 'issueKey');
}

function getJiraWorklogs(issueKeys) {
    const promises = issueKeys.map(issueKey => jira.getWorklogs(issueKey));
    return Promise.all(promises).then(results => {
        return issueKeys.reduce((memo, issueKey, idx) => {
            if (results[idx]) {
                memo[issueKey] = results[idx];
            }
            return memo;
        }, {});
    });
}

function filterTogglWorklogs(jiraWorklogs, togglWorklogs) {
    let jiraEntries = jiraWorklogs.reduce((result, worklog) => {
        const regex = /.+(\[[\d,\s]+])/g;
        const regexResult = regex.exec(worklog.comment);
        if (regexResult && regexResult.length) {
            try {
                const issueIds = JSON.parse(regexResult[1]);
                return result.concat(issueIds);
            } catch (e) {
                console.log('regex error', e);
                return result;
            }
        }
        return result;
    }, []);
    jiraEntries = _.uniq(jiraEntries);
    return togglWorklogs.filter(worklog => !jiraEntries.includes(worklog.id));
}

function createJiraWorklogs(newGroupedTogglWorklogs) {
    const promiseFns = [];
    let sumTime = 0;

    _.forOwn(newGroupedTogglWorklogs, (togglWorklogs, issueKey) => {
        togglWorklogs.forEach(worklog => {
            if (!worklog.duration) {
                return;
            }
            sumTime += worklog.duration;
            promiseFns.push(() => {
                return jira
                    .addWorklog(issueKey, {
                        comment: worklog.jiraComment,
                        started: moment(worklog.start).utc().format('YYYY-MM-DDTHH:mm:ss.SSSZZ'),
                        timeSpentSeconds: worklog.duration < 60 ? 60 : worklog.duration
                    })
                    .then(() => {
                        console.log(
                            chalk.green(`${checkmarks.heavy(true)}`) +
                                ` ${chalk.green(worklog.issueKey)} ${worklog.jiraCommentClean} - ` +
                                chalk.gray(`time spent: ${prettyMs(worklog.duration * 1000)}`)
                        );
                    });
            });
        });
    });

    return promiseFns.reduce((prevProm, nextProm) => prevProm.then(nextProm), Promise.resolve()).then(() => {
        if (!sumTime) {
            return;
        }
        console.log('');
        console.log(chalk.blue(`total time spent: ${prettyMs(sumTime * 1000)}`));
        console.log('');
    });
}

function init() {
    let groupedTogglWorklogs;
    console.log('');

    return getTogglWorklogs()
        .then(groupTogglWorklogsByIssueKey)
        .then(groupedWorklogs => (groupedTogglWorklogs = groupedWorklogs))
        .then(groupedWorklogs => getJiraWorklogs(Object.keys(groupedWorklogs)))
        .then(groupedJiraWorklogs => {
            const newGroupedTogglWorklogs = {};
            _.forOwn(groupedTogglWorklogs, (togglWorklogs, key) => {
                if (key in groupedJiraWorklogs) {
                    newGroupedTogglWorklogs[key] = filterTogglWorklogs(groupedJiraWorklogs[key] || [], togglWorklogs);
                }
            });
            return newGroupedTogglWorklogs;
        })
        .then(createJiraWorklogs);
}

init();
