const TogglClient = require('toggl-api');
const utils  = require('./utils');

module.exports = function(config) {
    const toggl = new TogglClient({ apiToken: config.toggl.token });

    return {
        // for today
        // eg. getWorklog(moment(new Date()).startOf('day').toDate(), moment().toDate()).then(console.log);

        getWorklog: function(start, end) {
            return new Promise(function(resolve, reject) {
                toggl.getTimeEntries(start, end, function(err, timeEntries) {
                    if (err) {
                        return reject(err);
                    }
                    timeEntries = timeEntries.map(timeEntry => utils.getIssueKey(timeEntry)).filter(timeEntry => {
                        const projectKey = timeEntry.issueKey.split('-')[0];
                        return config.projectKeys.includes(projectKey);
                    });
                    return resolve(timeEntries);
                });
            });
        }
    };
};
