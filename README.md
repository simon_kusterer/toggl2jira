# toggl2jira

## usage

```
toggl2jira
  --from '<date>' (by default 'today')
```

## libraries

* https://sugarjs.com/dates/
* https://github.com/wanasit/chrono
* https://github.com/sindresorhus/ora

## .toggl2jira.json

```JavaScript
{
  jira: {
    username: '',
    password: '',
    baseUrl: ''
  },
  toggl: {
    token: ''
  },
  projectKeys: ['BSLN', 'SDV']
}
```