const JiraClient = require('jira-client');
const url = require('url');
const _ = require('lodash');
const utils = require('./utils.js');

module.exports = function(config) {
    const jiraUrl = url.parse(config.jira.baseUrl);

    const jira = new JiraClient({
        protocol: jiraUrl.protocol,
        host: jiraUrl.hostname,
        username: config.jira.username,
        password: config.jira.password,
        apiVersion: '2',
        strictSSL: true
    });

  return {

    // get the worklogs from the current user
    getWorklogs(issueId) {
      return jira.doRequest({
        uri: jira.makeUri({
          pathname: `/issue/${issueId}/worklog`
        }),
        method: 'GET',
        'Content-Type': 'application/json',
        json: true
      })
      .then(({ worklogs }) => worklogs.filter((worklog) => _.get(worklog, 'author.name') === config.jira.username))
      .catch(() => false);
    },

    addWorklog(issueId, worklog) {
      return jira.doRequest({
        uri: jira.makeUri({
          pathname: `/issue/${issueId}/worklog`
        }),
        body: worklog,
        method: 'POST',
        'Content-Type': 'application/json',
        json: true,
      }).catch((err) => {
        console.log('addWorklog failed', err);
      });
    },

    getInstance() {
      return jira;
    }
  };
};
