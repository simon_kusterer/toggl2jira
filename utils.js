const _ = require('lodash');

function getIssueKey(togglWorklog) {
    const description = _.get(togglWorklog, 'description', '');
    const issueKey = description.split(/\s+/)[0];
    const jiraComment = description.substr(issueKey.length + 1) + ` [${togglWorklog.id}]`;
    const jiraCommentClean = description.substr(issueKey.length + 1);
    return Object.assign(togglWorklog, { issueKey, jiraComment, jiraCommentClean });
}

module.exports = {
    getIssueKey
};
